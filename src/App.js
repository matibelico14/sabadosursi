import React, { Component } from 'react';
import Header from './components/ui-components/header/Header';
import Body from './components/views/body/Body';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <Body/>
      </div>
    );
  }
}

export default App;
